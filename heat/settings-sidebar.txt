﻿1. [Hardaway](http://en.wikipedia.org/wiki/Tim_Hardaway)
2. [Mourning](http://en.wikipedia.org/wiki/Alonzo_Mourning)
3. [O'Neal](http://en.wikipedia.org/wiki/Shaquille_O'Neal)
4. [2006](http://en.wikipedia.org/wiki/2006_NBA_Finals)
5. [2012](http://en.wikipedia.org/wiki/2012_NBA_Finals)
6. [2013](http://en.wikipedia.org/wiki/2013_NBA_Finals)

##Flair Filter
[](https://www.reddit.com/r/heat/search?q=flair%3A%27post%20game%27&restrict_sr=on&t=all)[](https://www.reddit.com/r/heat/search?q=flair%3A%27twitter%27&restrict_sr=on&t=all)[](https://www.reddit.com/r/heat/search?q=flair%3A%27mod%20post%27&restrict_sr=on&t=all)[](https://www.reddit.com/r/heat/search?q=flair%3A%27game%20thread%27&restrict_sr=on&t=all)[](https://www.reddit.com/r/heat/search?q=flair%3A%27free%20talk%20friday%27&restrict_sr=on&t=all)[](https://www.reddit.com/r/heat/search?q=flair%3A%27prediction%27&restrict_sr=on&t=all)[](https://www.reddit.com/r/heat/search?q=flair%3A%27discussion%27&restrict_sr=on&t=all)[](https://www.reddit.com/r/heat/search?q=flair%3A%27article%27&restrict_sr=on&t=all)[]
(https://www.reddit.com/r/heat/search?q=flair%3A%27highlights%27&restrict_sr=on&t=all)

##Player Tracker

|From||To|
|---|---|---|
|[MIA](/r/Heat)|Chris Bosh *Waived*|[](/r/NBA)|
|[MIA](/r/Heat)|Udonis Haslem (UFA) *1 year, $2.3M (vet min.)*|[MIA](/r/Heat)|
|[MIA](/r/Heat)|Dion Waiters (UFA) *4 years, $52M*|[MIA](/r/Heat)|
|[MIA](/r/Heat)|James Johnson (UFA) *4 years, $60M*|[MIA](/r/Heat)|
|[MIA](/r/Heat)|Willie Reed (UFA) *1 year, $1.5M*|[LAC](/r/LAClippers)|
|[MIA](/r/Heat)|Luke Babbitt (UFA) *1 year, $1.9M*|[ATL](/r/atlantahawks)|
|[](/r/NBA)|Bam Adebayo *Drafted 14th*|[MIA](/r/Heat)|
|[BOS](/r/BostonCeltics)|Kelly Olynyk (UFA) *4 years, $50M*|[MIA](/r/Heat)|
|[MIA](/r/Heat)|Josh McRoberts *Traded (+ 2023 2nd)*|[DAL](/r/mavericks)|
|[DAL](/r/mavericks)|AJ Hammons *Traded*|[MIA](/r/Heat)|

##[Schedule](http://www.nba.com/heat/schedule/)

|Date|Matchup|Score|
|:--:|:--:|:--:|
|Sun, Oct  1 *6:00 PM*|[ATL](/r/AtlantaHawks)| *(Preseason)*|
|Thu, Oct  5 *7:30 PM*|[@ BKN](/r/gonets)| *(Preseason)*|
|Sat, Oct  7 *7:00 PM*|[@ ORL](/r/OrlandoMagic)| *(Preseason)*|
|Mon, Oct  9 *7:30 PM*|[CHA](/r/CharlotteHornets)| *(Preseason)*|
|Wed, Oct 11 *7:30 PM*|[WAS](/r/WashingtonWizards)| *(Preseason)*|
|Fri, Oct 13 *8:00 PM*|[@ PHI](/r/Sixers)| *(Preseason)*|
|Wed, Oct 18 *7:00 PM*|[@ ORL](/r/OrlandoMagic)||
|Sat, Oct 21 *8:00 PM*|[IND](/r/Pacers)||
|Mon, Oct 23 *7:30 PM*|[ATL](/r/AtlantaHawks)||
|Wed, Oct 25 *8:00 PM*|[SAS](/r/NBASpurs)||

##[Standings](http://espn.go.com/nba/standings/_/group/3)

||Team|W|L|PCT|
|:--:|:--|:--:|:--:|:--:|
||[Boston](/r/BostonCeltics)|0|0|.000|
||[Brooklyn](/r/gonets)|0|0|.000|
||[New York](/r/nyknicks)|0|0|.000|
||[Philadelphia](/r/Sixers)|0|0|.000|
||[Toronto](/r/TorontoRaptors)|0|0|.000|
||[Cleveland](/r/ClevelandCavs)|0|0|.000|
||[Chicago](/r/ChicagoBulls)|0|0|.000|
||[Milwaukee](/r/MkeBucks)|0|0|.000|
||[Indiana](/r/Pacers)|0|0|.000|
||[Detroit](/r/DetroitPistons)|0|0|.000|
||[Atlanta](/r/AtlantaHawks)|0|0|.000|
|****|**[Miami](/r/Heat)**|**0**|**0**|**.000**|
||[Orlando](/r/OrlandoMagic)|0|0|.000|
||[Washington](/r/WashingtonWizards)|0|0|.000|
||[Charlotte](/r/CharlotteHornets)|0|0|.000|

###Follow the Heat
**[](http://facebook.com/miamiheat)****[](http://twitter.com/miamiheat)****[](http://instagram.com/miamiheat)**


###Related Subs


[](http://nba.reddit.com)[](http://letsgofish.reddit.com)[](http://miamidolphins.reddit.com)


[](http://floridapanthers.reddit.com)[](http://miamimls.reddit.com)[](http://miamihurricanes.reddit.com) 


###Subreddit Rules


[Posting Guidelines Wiki Page](http://reddit.com/r/heat/wiki/postingguidlines)


|For questions, message the Mods|
|:--:|
|Please follow reddiquette.  |
|No comments or posts displaying discriminatory, racist, sexist, etc. language is allowed.|
|This subreddit stands against hate speech.^[†](http://redd.it/3djkz4)|
|Trolls will be banned and undesirable behavior will be removed at the discretion of the moderators.|
|Please refrain from re-posting submissions. Use the New filter.|
|Please report any spam or inappropriate links!|
|Posting links to news articles is ok, and posting a personal blog once in a while is alright, just don't get spammy with it or you will be banned from posting.|
|If your post isn't up within a few minutes, spam folder flagged it. Message the Mods.|


###### [Follow /r/Heat on Twitter ](https://twitter.com/Heat_Reddit) | [Player Salary Stats](https://www.reddit.com/r/heat/wiki/salary) | [Player Social Media](https://www.reddit.com/r/heat/wiki/socialmedia)
* [](http://stats.nba.com/teamStats.html?TeamID=1610612748)
* [](http://www.nba.com/players/josh/richardson/1626196)
* [](http://www.nba.com/players/chris/bosh/2547)
* [](http://www.nba.com/players/wayne/ellington/201961)
* [](http://www.nba.com/players/josh/mcroberts/201177)
* [](http://www.nba.com/players/luke/babbitt/202337)
* [](http://www.nba.com/players/goran/dragic/201609)
* [](http://www.nba.com/players/tyler/johnson/204020)
* [](http://www.nba.com/players/dion/waiters/203079)
* [](http://www.nba.com/heat/player-bios/okaro-white)
* [](http://www.nba.com/players/james/johnson/201949)
* [](http://www.nba.com/players/rodney/mcgruder/203585)
* [](http://www.nba.com/players/justise/winslow/1626159)
* [](http://www.nba.com/players/hassan/whiteside/202355)
* [](http://www.nba.com/players/willie/reed/203186)
* [](http://www.nba.com/players/udonis/haslem/2617)
